FROM centos:8 as build
RUN yum install -y gcc make tar
WORKDIR /build
RUN curl -L https://github.com/redis/redis/archive/6.0.6.tar.gz | tar -xz \
&& cd redis-6.0.6/src && make

FROM centos:8
COPY etc/redis /etc/redis
COPY --from=build /build/redis-6.0.6/src/redis-server /usr/local/bin/
COPY --from=build /build/redis-6.0.6/src/redis-sentinel /usr/local/bin/
COPY --from=build /build/redis-6.0.6/src/redis-cli /usr/local/bin/
COPY --from=build /build/redis-6.0.6/src/redis-check-rdb /usr/local/bin/
COPY --from=build /build/redis-6.0.6/src/redis-check-aof /usr/local/bin/
COPY --from=build /build/redis-6.0.6/src/redis-benchmark /usr/local/bin/
CMD ["redis-server", "/etc/redis/redis.conf"]
