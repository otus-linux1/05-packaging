# packaging

Процесс сборки rpm и создания репозитория описан в файлах script.sh и redis.spec

А докер-образа в докерфайле :shrug:
```
# можно собрать локально
docker build -t myredis .   

# можно взять с докерхаба
docker run --rm -it rbolkhovitin/myredis
```
