Name: redis
Version: %{version}
Release: %{release}
Summary: key-value storage
License: BSD 3
URL: https://github.com/redis/redis
Source0: %{name}-%{version}.tar.gz
Source1: etc
BuildRequires: gcc make
AutoReq: no

%define debug_package %{nil}

%description
Redis is often referred as a data structures server.
What this means is that Redis provides access to mutable data structures via a set of commands,
which are sent using a server-client model with TCP sockets and a simple protocol.
So different processes can query and modify the same data structures in a shared way.

%prep
%setup -q
cp -r %{SOURCE1} .

%build
cd src && make

%install
cp -r etc %{buildroot}/
cd src && make PREFIX=%{buildroot}/usr/local install

%pre
# create user if not exists
USER="redis"
getent group $USER >/dev/null || groupadd -r $USER
getent passwd $USER >/dev/null || useradd -r -g $USER $USER

%post
systemctl daemon-reload
systemctl enable %{name}
systemctl start %{name}

%preun
#only on uninstall, not on upgrades
if [ $1 = 0 ]; then
	systemctl stop %{name} && systemctl disable %{name}
fi

%files
/usr/local/bin/redis-server
/usr/local/bin/redis-cli
/usr/local/bin/redis-check-rdb
/usr/local/bin/redis-check-aof
/usr/local/bin/redis-benchmark
/usr/local/bin/redis-sentinel
/etc/systemd/system/redis.service

%config
/etc/redis/redis.conf

%doc
