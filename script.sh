#! /usr/bin/env bash

yum install -y gcc git rpm-build \
    createrepo rpmdevtools make \
    nginx  # vim mc tree

# build rpm with no-root user
cd /home/vagrant
sudo -uvagrant rpmdev-setuptree
sudo -uvagrant curl -L \
    https://github.com/redis/redis/archive/6.0.6.tar.gz \
    -o $(pwd)/rpmbuild/SOURCES/redis-6.0.6.tar.gz
sudo -uvagrant cp -r etc $(pwd)/rpmbuild/SOURCES/
sudo -uvagrant rpmbuild --clean --bb redis.spec \
    --define "version 6.0.6" \
    --define "release 1"

# change nginx config and start service
sed -ri 's:^(\s+)server \{:server \{\n\1\1autoindex on;:' /etc/nginx/nginx.conf 
systemctl enable nginx
systemctl start nginx

# create repo in nginx static directory
rm -rf /usr/share/nginx/html/*
mkdir /usr/share/nginx/html/x86_64/
cp /home/vagrant/rpmbuild/RPMS/x86_64/redis-6.0.6-1.x86_64.rpm \
  /usr/share/nginx/html/x86_64/
createrepo /usr/share/nginx/html

# add repo in yum config
cat <<EOF > /etc/yum.repos.d/my-repo.repo
[myrepo]
name=My Personal Repo
baseurl=http://localhost/$basearch
enabled=1
# not secure
gpgcheck=0
EOF

# update yum cache and install package
yum update
yum install -y --disablerepo="*" --enablerepo="myrepo" redis
